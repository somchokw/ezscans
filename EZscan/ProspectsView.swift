//
//  ProspectView.swift
//  EZscan
//
//  Created by nemesic on 17/6/2563 BE.
//  Copyright © 2563 nemesic. All rights reserved.
//

import UserNotifications
import CodeScanner
import SwiftUI

struct ProspectsView: View {
    
    enum FilterType {
        case none, contacted, uncontacted
    }
    
    @EnvironmentObject var prospects: Prospects
    @State private var isShowingScanner = false
    
    let filter: FilterType
    
//    var title: String {
//        switch filter {
//        case .none:
//            return "สถานที่ของฉัน"
//        case .contacted:
//            return "Contacted people"
//        case .uncontacted:
//            return "Uncontacted people"
//        }
//    }
    var titles = "สถานที่ของฉัน"
    
    var filteredProspects: [Prospect] {
        switch filter {
        case .none:
            return prospects.people
        case .contacted:
            return prospects.people.filter { $0.isContacted }
        case .uncontacted:
            return prospects.people.filter { !$0.isContacted }
        }
    }
    
   
    
    @State var place = ""
    
    var body: some View {
        NavigationView {
            
                 
            
            
                
                VStack {
                    TextField("ตั้งชื่อก่อนเพิ่มสถานที่",text: $place)
                        .font(.custom("Mitr-Medium", size: 24))
                        .textFieldStyle(MyTextFieldStyle())
                        
                    
                        
                        List {
                            
                            
                            ForEach(filteredProspects) { prospect in

                                NavigationLink(destination: myPlace(urls: "\(prospect.emailAddress)")) {
                                    VStack(alignment: .leading) {
                                        
                                        Text(prospect.namePlace)
                                        .font(.custom("Mitr-Medium", size: 24))
                                        Text(prospect.emailAddress)
                                            .foregroundColor(.secondary)
                                    }
                   
                                }
                            }.onDelete(perform: deleteItems)
                           
                        }
                
                    
                        .navigationBarTitle(titles)
                            .navigationBarItems(trailing: Button(action: {
                                self.isShowingScanner = true
                            }) {
                                Image(systemName: "qrcode.viewfinder")
                                Text("เพิ่มสถานที่").font(.custom("Mitr-Medium", size: 22))
                                
                            })
                        
                        .sheet(isPresented: $isShowingScanner) {
                            CodeScannerView(codeTypes: [.qr], simulatedData: "https://entercorp.net/", completion: self.handleScan)
                        }
                    
                
            }
        }.navigationViewStyle(StackNavigationViewStyle())
        
    }
    
   func deleteItems(at offsets: IndexSet) {
    let person = Prospect()
    prospects.people.remove(atOffsets: offsets)
    self.prospects.delete(person)
    }
    

    
    func handleScan(result: Result<String, CodeScannerView.ScanError>) {
       self.isShowingScanner = false
       switch result {
       case .success(let code):
           
        let person = Prospect()
           
           person.namePlace = place
           person.emailAddress = code
           self.prospects.add(person)
        
       case .failure(let error):
           print("Scanning failed")
       }    }

    

}

struct MyTextFieldStyle: TextFieldStyle {
    func _body(configuration: TextField<Self._Label>) -> some View {
        configuration
        .padding(30)
        .background(
            RoundedRectangle(cornerRadius: 20, style: .continuous)
                .stroke(Color.blue, lineWidth: 3)
        ).padding()
    }
}


struct ProspectsView_Previews: PreviewProvider {
    static let prospects = Prospects()
    static var previews: some View {
        ProspectsView(filter: .none).environmentObject(prospects)
        
    }
}
