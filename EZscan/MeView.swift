//
//  Meview.swift
//  EZscan
//
//  Created by nemesic on 17/6/2563 BE.
//  Copyright © 2563 nemesic. All rights reserved.
//
import CoreImage.CIFilterBuiltins
import SwiftUI

struct MeView: View {
    
    @State private var emailAddress = "https://entercorp.net/"
    
    let context = CIContext()
    let filter = CIFilter.qrCodeGenerator()
    
    var body: some View {
        NavigationView {
            
            ZStack {
//                LinearGradient(gradient: Gradient(colors: [.purple,.blue,.purple]), startPoint: .top, endPoint: .bottom).edgesIgnoringSafeArea(.all)
                VStack {
                    
                   

                    TextField("URL://", text: $emailAddress)
                        .textContentType(.URL)
                        .font(.title)
                        .padding([.horizontal, .bottom])

                    Image(uiImage: generateQRCode(from: "\(emailAddress)"))
                        .interpolation(.none)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 200, height: 200)
                    
                    Spacer()
                }
                .navigationBarTitle(" EnterCorporation ")
            }
        }
    }
    
    func generateQRCode(from string: String) -> UIImage {
        let data = Data(string.utf8)
        filter.setValue(data, forKey: "inputMessage")

        if let outputImage = filter.outputImage {
            if let cgimg = context.createCGImage(outputImage, from: outputImage.extent) {
                return UIImage(cgImage: cgimg)
            }
        }

        return UIImage(systemName: "xmark.circle") ?? UIImage()
    }
    
}

struct MeView_Previews: PreviewProvider {
    static var previews: some View {
        MeView()
    }
}
