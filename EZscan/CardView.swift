//
//  CardView.swift
//  EZscan
//
//  Created by nemesic on 23/6/2563 BE.
//  Copyright © 2563 nemesic. All rights reserved.
//

import SwiftUI

struct CardView: View {
    
    @Binding var symbol:String
    @Binding var background:Color
    
    var body: some View {
        Image(symbol)
            .resizable()
            .aspectRatio(1,contentMode: .fit)
            .background(background.opacity(0.5
            )).cornerRadius(20)
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(symbol: Binding.constant("tomato"),background: Binding.constant(Color.green))
    }
}
