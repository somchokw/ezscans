//
//  Prospect.swift
//  EZscan
//
//  Created by nemesic on 17/6/2563 BE.
//  Copyright © 2563 nemesic. All rights reserved.
//

import SwiftUI

class Prospect: Identifiable, Codable {
    let id = UUID()
    var namePlace = "1"
    var emailAddress = ""
    fileprivate(set) var isContacted = false
}

class Prospects: ObservableObject {
    @Published var people = [Prospect]()
    static let saveKey = "SavedData"

    init() {
        if let data = UserDefaults.standard.data(forKey: Self.saveKey) {
            if let decoded = try? JSONDecoder().decode([Prospect].self, from: data) {
                self.people = decoded
                return
            }
        }

        self.people = []
    }
    
    public func save() {
        if let encoded = try? JSONEncoder().encode(people) {
            UserDefaults.standard.set(encoded, forKey: Self.saveKey)
        }
    }
    
    
    
   
    
    func add(_ prospect: Prospect) {
        people.append(prospect)
        save()
    }
    
    func delete(_ prospect: Prospect){
        save()
    }
   
//    
//    func remove(_ prospect: Prospect) {
//        if let index = people.firstIndex(of: prospect) {
//            
//            people.remove(at: index)
//           save()
//        }
//        
//        
//        
//
//    }
//
//
//    func toggle(_ prospect: Prospect) {
//        objectWillChange.send()
//        prospect.isContacted.toggle()
//        save()
//    }
}
