//
//  ContentView.swift
//  EZscan
//
//  Created by nemesic on 17/6/2563 BE.
//  Copyright © 2563 nemesic. All rights reserved.
//

import SwiftUI



struct ContentView: View {
    
    var prospects = Prospects()
    
    var body: some View {
        
       TabView {
        

        ProspectsView(filter: .none)
               .tabItem {
                   Image(systemName: "qrcode")
                   Text("สถานที่ของฉัน")
               }
        
       
           gameView()
               .tabItem {
                   Image(systemName: "gamecontroller")
                   Text("เกมส์")
               }
        MeView()
        .tabItem {
            Image(systemName: "info.circle")
            Text("เกี่ยวกับเรา")
        }
       }.environmentObject(prospects)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
