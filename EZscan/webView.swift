//
//  webView.swift
//  EZscan
//
//  Created by nemesic on 18/6/2563 BE.
//  Copyright © 2563 nemesic. All rights reserved.
//

import SwiftUI
import Foundation
import WebKit

struct webView: UIViewRepresentable {
    var url: String
    func makeUIView(context: Context) -> WKWebView {
        guard let url = URL(string: self.url) else {
            return WKWebView()
        }
        
        let request = URLRequest(url: url)
        let wkWebview = WKWebView()
        wkWebview.load(request)
        return wkWebview
        
    }
    
    func updateUIView(_ uiView: webView.UIViewType, context: UIViewRepresentableContext<webView>) {
       
    }
    
}
